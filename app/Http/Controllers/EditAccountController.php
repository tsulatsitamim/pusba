<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\User;

class EditAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        return view('accounts.edit', compact('user'));
    }

    public function account(Request $request)
    {
        $user = Auth::user();

        $input = $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);

        $user->update([
            'name' => $input['name'],
            'email' => $input['email']
        ]);

        return redirect('/edit-account');
    }

    public function password(Request $request)
    {
        $user = Auth::user();

        $input = $this->validate(request(), [
            'oldPassword' => 'required',
            'password_confirmation' => 'required',
            'password' => ['required', 'confirmed'],

        ]);

        if (!Hash::check($request->oldPassword, Auth::user()->password))
        {
            return redirect()->back()->withErrors(['oldPassword' => ['You don\'t have permision to do this.']]);
        }

        $user->update([
            'password' => bcrypt($input['password'])
        ]);

        return redirect('/edit-account');
    
    }
}
