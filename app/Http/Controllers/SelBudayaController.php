<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\SelBudaya;
use App\BudayaKerja;
use App\Site;
use Illuminate\Support\Facades\Storage;

class SelBudayaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    function index(){
        $user = Auth::user();

        $selBudayas = $user->selBudayas
            ->concat(SelBudaya::where('chief', '=', $user->id)->get())
            ->concat(SelBudaya::where('cpsd', '=', $user->id)->get())
            ->concat(SelBudaya::where('creator', '=', $user->id)->get())
            ->unique()
            ->all();

        return view('sels.index', compact('selBudayas'));
    }

    function store(){

        $selBudaya = SelBudaya::create([
            'creator' => Auth::id()
        ]);

        $selBudaya->analisa()->save(new \App\Analisa);
        $selBudaya->perencanaan()->save(new \App\Perencanaan);
        $selBudaya->pelaksanaan()->save(new \App\Pelaksanaan);
        $selBudaya->evaluasi()->save(new \App\Evaluasi);

        return redirect('/sel-budaya/registrasi/' . $selBudaya->id );
    }

    function edit(Request $request, $id){
        $selBudaya = SelBudaya::with('budayaKerjas', 'members')->find($id);
        $sites = Site::all();
        $budayaKerjas = BudayaKerja::all();
        return view('sels.register', compact('sites', 'budayaKerjas', 'selBudaya'));
    }

    function update(Request $request, $id){

        $this->validate($request, [
            'team' => 'required',
            'chief' => 'required',
            'coach' => 'required',
            'cpsd' => 'required',
            'topic' => 'required',
            'title' => 'required'
        ]);

        $selBudaya = SelBudaya::find($id);

        $selBudaya->update([
            'reg_number' => 'SB/BDMA/' . Carbon::now()->year . '/' . sprintf("%03d", $id),
            'team' => $request->team,
            'chief' => $request->chief,
            'site_id' => $request->site,
            'coach' => $request->coach,
            'cpsd' => $request->cpsd,
            'topic' => $request->topic,
            'title' => $request->title,
            'progress' => 1,
            'status' => 1,
        ]);

        $budayaKerjas = BudayaKerja::all();
        $checked = array();

        foreach ($budayaKerjas as $budayaKerja) {
            if($request->{$budayaKerja->name}){
                array_push($checked, $budayaKerja->id);
            }
        }

        $selBudaya->budayaKerjas()->sync($checked);

        return redirect('/');
    }

    function view($id)
    {
        $selBudaya = SelBudaya::find($id);
        $budayaKerjas = BudayaKerja::all();

        return view('sels.view', compact('selBudaya', 'budayaKerjas'));
    }

    function destroy($id)
    {
        $selBudaya = SelBudaya::find($id);
        $documents = $selBudaya->document();

        foreach ($documents as $document) {
            Storage::delete($document->path);
        }

        $selBudaya->delete();

        return back();

    }
}
