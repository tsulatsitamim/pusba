<!--begin::Portlet-->
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide">
          <i class="la la-gear"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Registrasi
        </h3>
      </div>
    </div>
  </div>
  <!--begin::Form-->
  <form class="m-form m-form--fit m-form--label-align-right">

    {{ csrf_field() }}
    <div class="m-portlet__body">
      <input id="id-sb" name="chief" type="hidden" value="{{$selBudaya->id}}">                  
      <div class="form-group m-form__group">
        <label>
          Nomer Registrasi
        </label>
        <input type="text" class="form-control m-input" value="{{$selBudaya->reg_number}}" disabled="disabled">
      </div>
      <div class="form-group m-form__group">
        <label>
          Nama Tim
        </label>
        <input type="text" class="form-control m-input" value="{{$selBudaya->team}}" disabled="disabled">
      </div>

      <div class="form-group m-form__group">
        <label>
          Site
        </label>
        <input type="text" class="form-control m-input" value="{{$selBudaya->site->name}}" disabled="disabled">
      </div>

      <div class="form-group m-form__group">
        <label>
          Ketua
        </label>
        <input type="text" class="form-control m-input" value="{{displayNameId($selBudaya->chief)}}" disabled="disabled">
      </div>

      <div class="form-group m-form__group">
        <label>
          Pelatih Utama
        </label>
        <input type="text" class="form-control m-input" value="{{displayNameId($selBudaya->coach)}}" disabled="disabled">
      </div>

      <div class="form-group m-form__group">
        <label>
          CPSD
        </label>
        <input type="text" class="form-control m-input" value="{{displayNameId($selBudaya->cpsd)}}" disabled="disabled">
      </div>

      <div class="form-group m-form__group">
        <label for="members">
          Members
        </label>
          <div class="m-form__group m-list-timeline">
            <div class="list-members m-list-timeline__items">
              @foreach ($selBudaya->members as $member)
                <div class="m-list-timeline__item">
                  <span class="m-list-timeline__badge m-list-timeline__badge--primary"></span>
                  <span class="m-list-timeline__text">
                    {{displayNameId($member->id)}}
                  </span>
                </div>
              @endforeach                            
            </div>
          </div>
      </div>

      <div class="m-form__group form-group">
        <label for="">
          Nilai Inti Budaya
        </label>
        <div class="m-checkbox-list">
          @foreach($budayaKerjas as $budayaKerja)
            <label class="m-checkbox m-checkbox--bold m-checkbox--disabled m-checkbox--state-brand">
              <input type="checkbox"  name="{{$budayaKerja->name}}" {{checkOption($selBudaya->budayaKerjas, 'name', $budayaKerja->name)}} disabled>
              {{ $budayaKerja->name }}
              <span></span>
            </label>
          @endforeach
        </div>
      </div>

      <div class="form-group m-form__group">
        <label>
          Topik
        </label>
        <textarea class="form-control m-input" rows="3" disabled>{{$selBudaya->topic}}</textarea>
      </div>

      <div class="form-group m-form__group">
        <label>
          Judul
        </label>
        <textarea class="form-control m-input" rows="3" disabled>{{$selBudaya->title}}</textarea>
      </div>

    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
      {{--  <div class="m-form__actions">

      </div>  --}}
    </div>
  </form>

  <!--end::Form-->
</div>
<!--end::Portlet-->