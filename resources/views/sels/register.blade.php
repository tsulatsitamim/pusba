@extends('layouts.body')

@section('content')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Sel Budaya
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
          
          <div class="col-lg-6">
            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Registrasi
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="" id="new-sb">

                {{ csrf_field() }}
                <div class="m-portlet__body">
                  <input id="id-sb" name="chief" type="hidden" value="{{$selBudaya->id}}">                  
                  <div class="form-group m-form__group">
                    <label for="team">
                      Nama Tim
                    </label>
                    <input type="text" class="form-control m-input" id="name" placeholder="Nama Tim"  name="team" value="{{$selBudaya->team}}">
                    @include('layouts.errors-form', ['field' => 'team'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="site">
                      Site
                    </label>
                    <select class="form-control m-input" id="site" name="site">
                      @foreach($sites as $site)
                        <option {{$selBudaya->site_id == $site->id ? 'selected' : '' }} value="{{$site->id}}">
                          {{ $site->name }}
                        </option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group m-form__group">
                    <label for="chief">
                      Ketua
                    </label>
                    <input id="chief" type="text" class="form-control" placeholder="Ketua" value="{{displayNameId($selBudaya->chief)}}">
                    <input id="input-chief" name="chief" type="hidden" value="{{$selBudaya->chief}}">
                    @include('layouts.errors-form', ['field' => 'chief'])                    
                  </div>

                  <div class="form-group m-form__group">
                    <label for="coach">
                      Pelatih Utama
                    </label>
                    <input id="coach" type="text" class="form-control" placeholder="Pelatih" value="{{displayNameId($selBudaya->coach)}}">
                    <input id="input-coach" name="coach" type="hidden" value="{{$selBudaya->coach}}">
                    @include('layouts.errors-form', ['field' => 'coach'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="cpsd">
                      CPSD
                    </label>
                    <input id="cpsd" type="text" class="form-control" placeholder="CPSD" value="{{displayNameId($selBudaya->cpsd)}}">
                    <input id="input-cpsd" name="cpsd" type="hidden" value="{{$selBudaya->cpsd}}">
                    @include('layouts.errors-form', ['field' => 'cpsd'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="members">
                      Members
                    </label>
                    <input id="members" type="text" class="form-control" placeholder="Members" value="">

                        <div class="m-form__group m-list-timeline">
                          <div class="list-members m-list-timeline__items">
                            @foreach ($selBudaya->members as $member)
                              <div class="m-list-timeline__item">
                                <span class="m-list-timeline__badge m-list-timeline__badge--primary"></span>
                                <span class="m-list-timeline__text">
                                  {{displayNameId($member->id)}}
                                </span>
                                <span class="m-list-timeline__time">
                                  <button type="button" class="remove-member btn m-btn m-btn--hover-danger m-btn--icon btn-sm m-btn--icon-only m-btn--pill" title="Delete">
                                    <i class="la la-trash"></i>
                                  </button>
                                  <input class="user-id" name="user-id" type="hidden" value="{{$member->id}}">
                                </span>
                              </div>
                            @endforeach                            
                          </div>
                        </div>

                  </div>

                  <div class="m-form__group form-group">
                    <label for="">
                      Nilai Inti Budaya
                    </label>
                    <div class="m-checkbox-list">
                      @foreach($budayaKerjas as $budayaKerja)
                        <label class="m-checkbox">
                          <input type="checkbox"  name="{{$budayaKerja->name}}" {{checkOption($selBudaya->budayaKerjas, 'name', $budayaKerja->name)}}>
                          {{ $budayaKerja->name }}
                          <span></span>
                        </label>
                      @endforeach
                    </div>
                  </div>

                  <div class="form-group m-form__group">
                    <label for="topic">
                      Topik
                    </label>
                    <textarea name="topic" class="form-control m-input" id="topic" rows="3">{{$selBudaya->topic}}</textarea>
                    @include('layouts.errors-form', ['field' => 'topic'])
                  </div>

                  <div class="form-group m-form__group">
                    <label for="title">
                      Judul
                    </label>
                    <textarea name="title" class="form-control m-input" id="title" rows="3">{{$selBudaya->title}}</textarea>
                    @include('layouts.errors-form', ['field' => 'title'])
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="new-sb">
                      Proses
                    </button>
                    <a href="/sel-budaya" class="btn btn-danger m-btn m-btn--custom float-left" role="button">Batal</a>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>
          
          {{--  <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Edit Password
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/edit-account/password" id="edit-password">
                {{ csrf_field() }}
                <div class="m-portlet__body">

                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      Old password
                    </label>
                    <input type="password" class="form-control m-input" id="oldPassword" placeholder="Password" name="oldPassword">
                    @include('layouts.errors-form', ['field' => 'oldPassword'])                  
                  </div>
                  
                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      New password
                    </label>
                    <input type="password" class="form-control m-input" id="password_confirmation" placeholder="Password" name="password_confirmation">
                    @include('layouts.errors-form', ['field' => 'password_confirmation'])                                    
                  </div>

                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      Password confirmation
                    </label>
                    <input type="password" class="form-control m-input" id="password" placeholder="Password" name="password">
                    @include('layouts.errors-form', ['field' => 'password'])                                    
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="edit-password">
                      Save
                    </button>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>  --}}

        </div>
      </div>
  </div>
@endsection

@section('contentmodal')

@endsection

@section('contentscript')

  @include ('footer')

  <script src="/js/autocomplete.min.js"></script>

  <script>
    $(document).ready(function() {

      $('#chief').autocomplete({
        serviceUrl: '/daftar-karyawan',
        onSelect: function (suggestion) {
          $('#input-chief').prop('value', suggestion.data);
        },
        maxHeight: 100
      });

      $('#coach').autocomplete({
        serviceUrl: '/daftar-karyawan',
        onSelect: function (suggestion) {
          $('#input-coach').prop('value', suggestion.data);
        },
        maxHeight: 100
      });

      $('#cpsd').autocomplete({
        serviceUrl: '/daftar-karyawan',
        onSelect: function (suggestion) {
          $('#input-cpsd').prop('value', suggestion.data);
        },
        maxHeight: 100
      });

      $('#members').autocomplete({
        serviceUrl: '/daftar-karyawan',
        onSelect: function (suggestion) {
          $('#members').prop('value', '');        
          adduser(suggestion.value, suggestion.data);
        },
        maxHeight: 100,
        onHide: function (suggestion) {
          $('#members').prop('value', '');
        }
      });

      $('.list-members').on('click', '.remove-member', function(){
        userId = $(this).next().val();
        el = $(this).closest('.m-list-timeline__item');
        removeUser(userId, el);
      });

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    });

    function adduser(value, id){
      $.ajax({
        type: 'POST',
        data: {
          selBudaya: $('#id-sb').val(),
          userId: id
        },
        dataType : "json",
        url: '/sel-budaya/member'
      }).done(function(data) {
          $('.list-members').append(`
            <div class="m-list-timeline__item">
              <span class="m-list-timeline__badge m-list-timeline__badge--primary"></span>
              <span class="m-list-timeline__text">` +
                value
              +`</span>
              <span class="m-list-timeline__time">
              <button type="button" class="remove-member btn m-btn m-btn--hover-danger m-btn--icon btn-sm m-btn--icon-only m-btn--pill" title="Delete">
                  <i class="la la-trash"></i>
              </button>
              <input class="user-id" name="user-id" type="hidden" value="` + id + `">              
              </span>
            </div>
          `);
          console.log(data.status);
      }).fail(function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus + ': ' + errorThrown);
      });
    }

    function removeUser(userId, el){
      $.ajax({
        type: 'GET',
        url: '/sel-budaya/' + $('#id-sb').val() + '/member/' + userId
      }).done(function(data) {
          animateRemove(el);
          console.log(data.status);
      }).fail(function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus + ': ' + errorThrown);
      });
    }

    function animateRemove(el) {
      el.animate({opacity: '0'}, 150, function(){
        el.animate({height: '0px'}, 150, function(){
          el.remove();
        });
      });
    }

  </script>

@endsection
