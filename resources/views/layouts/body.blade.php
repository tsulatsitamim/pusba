@extends('layouts.core')

@section('body')

	<body class="m-page--fluid m--skin- m-page--loading m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin::Page loader -->
		<div class="m-page-loader">
			<div class="m-spinner m-spinner--brand"></div>
		</div>
		<!-- end::Page Loader -->

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- begin: Header -->
      @include('layouts.header')
			<!-- END: Header -->		

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
        @include('layouts.leftaside')
				<!-- END: Left Aside -->
        @yield('content')

			</div>
			<!-- end:: Body -->


			<!-- begin::Footer -->
			@include('layouts.footer')
			<!-- end::Footer -->
		</div>
		<!-- end:: Page -->

		<!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->

		<!--begin::Modal-->
		@yield('contentmodal')
		<!--end::Modal-->


		<!--begin::Base Scripts -->
		<script src="/js/vendors.bundle.js" type="text/javascript"></script>
		<script src="/js/scripts.bundle.js" type="text/javascript"></script>
		<script src="/js/loader.js" type="text/javascript"></script>
		<script src="/js/app.js" type="text/javascript"></script>
		<script src="/amcharts/plugins/export/export.min.js"></script>

		<!--end::Base Scripts -->   
	
		@yield('contentscript')
		
		<script>
			$(window).on('load', function() {
					$('body').removeClass('m-page--loading');         
			});				

			jQuery(document).ready(function() {

				uri2 = $("#data-url").attr("data-url");

				var chart2 = AmCharts.makeChart("chartdiv2", {
						"path": "/amcharts/",
						"type": "gantt",
						"theme": "light",
						"marginRight": 70,
						"period": "DD",
						"dataDateFormat": "YYYY-MM-DD",
						"columnWidth": 0.5,
						"valueAxis": {
								"type": "date"
						},
						"brightnessStep": 7,
						"graph": {
								"fillAlphas": 1,
								"lineAlpha": 1,
								"lineColor": "#fff",
								"fillAlphas": 0.85,
								"balloonText": "<b>[[task]]</b>:<br />[[open]] -- [[value]]"
						},
						"rotate": true,
						"categoryField": "category",
						"segmentsField": "segments",
						"colorField": "color",
						"startDateField": "start",
						"endDateField": "end",
						"dataLoader": {
								"url": uri2,
								"format": "json"
						},          
						"valueScrollbar": {
								"autoGridCount": true
						},
						"chartCursor": {
								"cursorColor": "#55bb76",
								"valueBalloonsEnabled": false,
								"cursorAlpha": 0,
								"valueLineAlpha": 0.5,
								"valueLineBalloonEnabled": true,
								"valueLineEnabled": true,
								"zoomable": false,
								"valueZoomable": true
						},

						"export": {
								"enabled": true
						}
				});

			});
		</script>
		
		<!-- end::Page Loader -->
	</body>
	<!-- end::Body -->
@endsection