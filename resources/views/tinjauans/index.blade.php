@extends('layouts.body')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">
                    Dashboard
                </h3>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Tinjauan
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <a href="/sel-budaya/registrasi" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>
                                        Sel Budaya
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th data-field="Nomer Registrasi"></th>
                            <th data-field="Tim"></th>
                            <th data-field="Ketua"></th>
                            <th data-field="Pelatih Utama"></th>
                            <th data-field="Tahap"></th>
                            <th data-field="Tinjau"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($selBudayas as $selBudaya)  
                            <tr>
                                <td>
                                    {{ $selBudaya->reg_number ? $selBudaya->reg_number : '-' }}
                                </td>
                                <td>
                                    {{ $selBudaya->team ? $selBudaya->team : '-' }}
                                    
                                </td>
                                <td>
                                    {{ $selBudaya->chief()->first() ? displayName($selBudaya->chief()->first()) : '-' }}
                                </td>
                                <td>
                                    {{ $selBudaya->coach()->first() ? displayName($selBudaya->coach()->first()) : '-' }}
                                </td>
                                <td>
                                    <span>
                                        <span class="m-badge  m-badge--{{ $selBudaya->progress == 6 ? 'success' : 'metal' }} m-badge--wide">{{ sbProgress($selBudaya->progress, 0) }}</span>
                                    </span>
                                </td>
                                <td>
                                    <span>
                                        <a href="/{{ eyeSlash($selBudaya->status) ? 'sel-budaya/view' : 'tinjauan' }}/{{$selBudaya->id}}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="{{ eyeSlash($selBudaya->status) ? 'Lihat' : 'Tinjau' }}">
                                            <i class="la la-eye{{ eyeSlash($selBudaya->status) ? '-slash' : '' }}"></i>
                                        </a>
                                    </span>
                                </td>
                                <td>

                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
        
    </div>
</div>


@endsection
 
@section('contentmodal')

@endsection

@section('contentscript')
    <script>
   
        var DatatableHtmlTableDemo = function() {
        //== Private functions

            // demo initializer
            var demo = function() {

                var datatable = $('.m-datatable').mDatatable({
                    data: {
                        saveState: {cookie: false},
                    },
                    search: {
                        input: $('#generalSearch'),
                    },
                    columns: [
                        {
                            field: 'Nomer Registrasi',
                            width: 140,
                            textAlign: 'center',
                            sortable: !1
                        },
                        {
                            field: 'Tim',
                            width: 140
                            
                        },
                        {
                            field: 'Tahap',
                            width: 140,
                            textAlign: 'center'
                            
                        },
                        {
                            field: 'Tinjau',
                            width: 70,
                            textAlign: 'center'
                            
                        }
                    ]
                });
            };

            return {
                //== Public functions
                init: function() {
                // init dmeo
                demo();
                },
            };
        }();

        jQuery(document).ready(function() {
        DatatableHtmlTableDemo.init();
        });
        
    </script>

    @include ('footer')

@endsection