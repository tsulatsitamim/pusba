  <div class="m-grid__item m-grid__item--fluid m-wrapper">
      <div class="m-subheader ">
          <div class="d-flex align-items-center">
              <div class="mr-auto">
                  <h3 class="m-subheader__title ">
                      Dashboard
                  </h3>
              </div>
          </div>
      </div>
      <!-- END: Subheader -->
      <div class="m-content">
        <div class="row">
          <div class="col-lg-6 tinjauan">
            @includeWhen($selBudaya->progress > 0, 'displays.register')
            @includeWhen($selBudaya->progress > 1, 'displays.analisa')
            @includeWhen($selBudaya->progress > 2, 'displays.perencanaan')
            @includeWhen($selBudaya->progress > 3, 'displays.pelaksanaan')
            @includeWhen($selBudaya->progress > 4, 'displays.evaluasi')
            <!--begin::Portlet-->
            {{--  <div class="m-portlet m-portlet--tab">
              <form class="m-form m-form--fit m-form--label-align-right">

                {{ csrf_field() }}

                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="accept">
                      Setujui
                    </button>
                    <button type="submit" class="btn btn-danger m-btn m-btn--custom float-left" form="reject">
                      Tolak
                    </button>
                  </div>
                </div>
              </form>
              <form action="/tinjauan/{{$selBudaya->id}}" id="accept" method="post">
                {{ csrf_field() }}
                <input name="status" type="hidden" value="1">
              </form>
              <form action="/tinjauan/{{$selBudaya->id}}" id="reject" method="post">
                {{ csrf_field() }}
                <input name="status" type="hidden" value="0">
              </form>
              <!--end::Form-->
            </div>  --}}
            <!--end::Portlet-->
          </div>
          
          {{--  <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                      Edit Password
                    </h3>
                  </div>
                </div>
              </div>
              <!--begin::Form-->
              <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="/edit-account/password" id="edit-password">
                {{ csrf_field() }}
                <div class="m-portlet__body">

                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      Old password
                    </label>
                    <input type="password" class="form-control m-input" id="oldPassword" placeholder="Password" name="oldPassword">
                    @include('layouts.errors-form', ['field' => 'oldPassword'])                  
                  </div>
                  
                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      New password
                    </label>
                    <input type="password" class="form-control m-input" id="password_confirmation" placeholder="Password" name="password_confirmation">
                    @include('layouts.errors-form', ['field' => 'password_confirmation'])                                    
                  </div>

                  <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">
                      Password confirmation
                    </label>
                    <input type="password" class="form-control m-input" id="password" placeholder="Password" name="password">
                    @include('layouts.errors-form', ['field' => 'password'])                                    
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <button type="submit" class="btn btn-accent m-btn m-btn--custom float-right" form="edit-password">
                      Save
                    </button>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>  --}}

        </div>
      </div>
  </div>

  @include ('footer')
