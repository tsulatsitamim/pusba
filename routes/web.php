<?php
use Illuminate\Http\Request;
use App\SelBudaya;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/edit', function(){

    return view('accounts.edit');
}
);

Route::redirect('/', '/sel-budaya');
Route::get('/sel-budaya', 'SelBudayaController@index');
Route::get('/sel-budaya/registrasi', 'SelBudayaController@store');
Route::get('/sel-budaya/registrasi/{id}', 'SelBudayaController@edit');
Route::post('/sel-budaya/registrasi/{id}', 'SelBudayaController@update');
Route::get('/sel-budaya/view/{id}', 'SelBudayaController@view');
Route::get('/sel-budaya/destroy/{id}', 'SelBudayaController@destroy');

Route::get('/sel-budaya/analisa/{id}', 'AnalisaController@edit');
Route::post('/sel-budaya/analisa/{id}', 'AnalisaController@update');

Route::get('/sel-budaya/perencanaan/{id}', 'PerencanaanController@edit');
Route::post('/sel-budaya/perencanaan/{id}', 'PerencanaanController@update');
Route::get('/sel-budaya/time/{id}', 'PerencanaanController@getTime');
Route::get('/sel-budaya/time2/{id}', 'PerencanaanController@getTime');
Route::post('/sel-budaya/time/{id}', 'PerencanaanController@time');
Route::post('/sel-budaya/time2/{id}', 'PerencanaanController@time2');

Route::get('/sel-budaya/pelaksanaan/{id}', 'PelaksanaanController@edit');
Route::post('/sel-budaya/pelaksanaan/{id}', 'PelaksanaanController@update');

Route::get('/sel-budaya/evaluasi/{id}', 'EvaluasiController@edit');
Route::post('/sel-budaya/evaluasi/{id}', 'EvaluasiController@update');

Route::get('/tinjauan', 'TinjauanController@index');
Route::get('/tinjauan/{id}', 'TinjauanController@tinjau');
Route::get('/tinjauan/pdf/{id}', 'TinjauanController@pdf');
Route::get('/tinjauan/pdf/view/{id}', 'TinjauanController@pdf2');
Route::post('/tinjauan/{id}', 'TinjauanController@edit');

Route::post('/sel-budaya/upload/{id}', 'DocumentController@store');
Route::get('/sel-budaya/upload/delete/{id}', 'DocumentController@destroy');



Route::get('/manage-accounts', 'ManageAccountController@index');
Route::post('/manage-accounts', 'ManageAccountController@store');
Route::post('/manage-accounts/edit/{user}', 'ManageAccountController@update');
Route::get('/manage-accounts/delete/{user}', 'ManageAccountController@destroy');
Route::get('/manage-accounts/reset/{user}', 'ManageAccountController@resetPassword');

Route::get('/edit-account', 'EditAccountController@index');
Route::post('/edit-account/edit', 'EditAccountController@account');
Route::post('/edit-account/password', 'EditAccountController@password');


Route::get('/daftar-karyawan', function(Request $request){
    
    $query = $request->input('query');

    $users = DB::table('users')
        ->where('users.nrp', 'like', "%$query%")
        ->orWhere('users.name', 'like', "%$query%")
        ->join('sites', 'users.site_id', '=', 'sites.id')
        ->select('nrp as nrp', 'users.name as value', 'sites.name as site', 'users.id as uid')
        ->get()        
        ->map(function($item){
            return array('value' => $item->nrp . ' - ' . $item->site . ' - ' . $item->value, 'data' => $item->uid);
        });

    return response()->json(['suggestions'=>$users]);

});

Route::get('/sel-budaya/{id}/member/{userId}', function(Request $request, $id, $userId){
    $detach = App\SelBudaya::find($id)->members()->detach($userId);
    if($detach){
        return response()->json([
            'status' => 'deleted'
        ]);
    }

    return response()->json([
        'status' => 'something wrong'
    ]);
});

Route::post('/sel-budaya/member', function(Request $request){

    App\SelBudaya::find($request->selBudaya)->members()->attach($request->userId);

    return response()->json([
        'status' => 'attached'
    ]);
});