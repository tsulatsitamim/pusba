<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analisas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('analysis')->nullable();
            $table->string('behaviour')->nullable();
            $table->string('impact')->nullable();
            $table->integer('sel_budaya_id')->unsigned();
            $table->timestamps();
            $table->foreign('sel_budaya_id')->references('id')->on('sel_budayas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analisas');
    }
}
